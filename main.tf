data "ignition_link" "backup_timer" {
  path = "${var.home}/.config/systemd/user/timers.target.wants/podman-volume-backup@systemd-grafana.timer"
  target = "${var.home}/.config/systemd/user/podman-volume-backup@.timer"
  hard = false
}

data "ignition_file" "volume_unit" {
  path = "${var.home}/.config/containers/systemd/grafana.volume"
  content {
    content = file("${path.module}/templates/grafana.volume")
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "container_unit" {
  path = "${var.home}/.config/containers/systemd/grafana.container"
  content {
    content = templatefile("${path.module}/templates/grafana.container", {
      publish_host = var.publish_host
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_config" "config" {
  links = [
    data.ignition_link.backup_timer.rendered
  ]

  files = [
    data.ignition_file.volume_unit.rendered,
    data.ignition_file.container_unit.rendered
  ]
}
